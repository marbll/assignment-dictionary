asm = nasm
asmFlags = -f elf64 -o
ld = ld
ldFlags = -o

.PHONY = clean
.DEFAULT_GOAL = main

lib.o: lib.asm
	$(asm) $(asmFlags) $@ $<

dict.o: dict.asm lib.o
	$(asm) $(asmFlags) $@ $<

main.o: main.asm lib.o dict.o words.inc colon.inc
	$(asm) $(asmFlags) $@ $<

main: main.o lib.o dict.o
	$(ld) $(ldFlags) $@ $^

clean:
	rm lib.o dict.o main.o main
