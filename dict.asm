%define LABEL_SIZE 8

extern string_equals
global find_word

section .text
find_word:          ;rdi - указатель на строку, rsi - указатель на начало словаря
     .loop:         ;Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь - rax, иначе вернёт 0 - rax.
         cmp rsi, 0
         jz .end
         add rsi, LABEL_SIZE     ;сдвиг на 8 байт потому что первые 8 байт занимает метка, а потом уже ключ
         call string_equals
         cmp rax, 1
         je .search_end
         sub rsi, LABEL_SIZE
         mov rsi, [rsi]
         jmp .loop
     .search_end:
        mov rax, rsi
        ret
     .end:
        xor rax, rax
        ret



       