%define BUFFER_SIZE 255
%define STDOUT 1
%define STDERR 2

extern print_string
extern exit
extern find_word
extern print_newline
extern string_length
extern read_line
%include "words.inc"

section .rodata
overflow_error: db "not enough buffer size, word reading failed :(", 0
error_message: db "this word wasn't found", 0

section .text

global _start

_start:
    mov r10, rsp
    mov rax, rdi
    sub rsp, BUFFER_SIZE
    mov rdi, rsp
    mov rsi, BUFFER_SIZE
    call read_line
    mov rdi, rax
    cmp rax, 0
    je .buffer_overflow
    mov rdi, rax
    mov rsi, last
    call find_word
    cmp rax, 0
    jz .not_found
    mov rdi, rax
    call string_length
    add rdi, rax
    inc rdi
    mov rsi, STDOUT
    call print_string
    jmp .end
    .buffer_overflow:
        mov rdi, overflow_error
        mov rsi, STDERR
        call print_string
        jmp .end
    .not_found:
        mov rdi, error_message
        mov rsi, STDERR
        call print_string
    .end:
        call print_newline
        add rsp, BUFFER_SIZE
        call exit
